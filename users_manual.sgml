<!doctype debiandoc system>

<!--
 Debian User's Manual
 Copyright (C)1997 Christian Schwarz;
 released under the terms of the GNU
 General Public License, version 2 or (at your option) any later.
 -->

<book>

<title>Debian User's Manual
<author>Christian Schwarz <email/schwarz@debian.org/
<author>based on the work of Ian Jackson <email/ijackson@gnu.ai.mit.edu/
<version>version 0.1, <date>

<abstract>
(I need something really good here)
</abstract>

<copyright>Copyright &copy;1997 Christian Schwarz.
<p>

This manual is free software; you may redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation; either version 2, or (at your option) any
later version.
<p>

This is distributed in the hope that it will be useful, but
<em>without any warranty</em>; without even the implied warranty of
merchantability or fitness for a particular purpose.  See the GNU
General Public License for more details.
<p>

You should have received a copy of the GNU General Public License with
your Debian GNU/Linux system, in <tt>/usr/doc/copyright/GPL</tt>, or
with the <prgn/dpkg/ source package as the file <tt>COPYING</tt>.  If
not, write to the Free Software Foundation, Inc., 675 Mass Ave,
Cambridge, MA 02139, USA.

<toc sect>

<chapt id="scope">About this reference
<p>

<chapt>The bug tracking system
<p>

<sect>Reporting a bug
<p>

<sect1>The procedure
<p>
If you want to report a bug (this can be a bug in any part of the
Debian GNU/Linux system, e.g. in a document or a package), please
send a mail to <tt>submit@bugs.debian.org</tt>.
<p>
The bug report will
be automatically processed by a program, postet to the
<tt>debian-bugs-dist</tt> mailing list (you can prevent this, see below),
and forwarded to the responsible
maintainer. Therefore, the bug report should have a pseudo-header at
the top so that the bug tracking system can assign the bug to the
specified package and can determine the maintainer.
<p>
The pseudo-header for a bug report against the package <tt>foo</tt>
with version <tt>1.0-1</tt> would look like this:
<example>
Package: foo
Version: 1.0-1

</example>
As it's called `header' it has to be placed at the top of the email.
(Note, that the bug tracking system does not handle MIME emails correct
currently. For example, you should not sign your bug report with PGP as
the PGP seperator line would be treated as a malformed header.)
<p>
The bug tracking system knows several <em>pseudo packages</em> for
non-package parts of Debian. These are:
<taglist>
<tag><tt>ftp.debian.org</tt>
<item>the Debian archive and FTP server
<tag><tt>bugs.debian.org</tt>
<item>the bug tracking system itself
</taglist>
[to be completed, Ian wrote that the complete list can be accessed
over the www interface of the bug tracking system]
<p>

If you have a buggy program you want to report a bug against then
you'll have to determine the package name and version
for the program first.
You can get these using <tt>dpkg --search</tt> and <tt>dpkg --list</tt>;
see <tt>dpkg --help</tt> for a further explanation.
<p>   

<sect1>The contents of a bug report
<p>

Your bug report should contain the following pieces of information:
<list>
<item> The <em>exact</em> and <em>complete</em> text of any error
messages printed or logged. This is very important!
<item> Exactly what you typed or did to demonstrate the problem.
<item> A description of the incorrect behaviour: exactly what behaviour
       you were expecting, and what you observed. A transcript of an
       example session is a good way of showing this.
<item> A suggested fix, or even a patch, if you have one.
<item> Details of the configuration of the program with the problem.
       Include the complete text of its configuration files.
<item> The version of the whole Debian system you are using.
<item> What kernel version you're using (type uname -a).
<item> What shared C library you're using (type ls -l /lib/libc.so.5).
<item> Any other details of your Linux system, if it seems appropriate.
       For example, if you had a problem with a Debian Perl script, you
       would want to provide the version of the `perl' binary (perl -v).
<item> Appropriate details of the hardware in your system. If you're
       reporting a problem with a device driver please list <em>all</em>
       the hardware in your system, as problems are often caused by IRQ and
       I/O address conflicts.
<item>
Include any detail that seems relevant - you are in very little danger
   of making your report too long by including too much information. If
   they are small please include in your report any files you were using
   to reproduce the problem (uuencoding them if they may contain odd
   characters etc.).
<p>   
</list>
<p>

   Please don't report several unrelated bugs - especially ones in
   different packages - in one message. Also, please don't mail your bug
   report to any mailing lists or recipients other than
   <tt>submit@bugs.debian.org</tt>.
<p>   
    
<sect1>An example
<p>
A bug report, with mail header, looks something like this:
<example>
  To: submit@bugs.debian.org
  From: diligent@testing.linux.org
  Subject: Hello says `goodbye'

  Package: hello
  Version: 1.3-2

  When I invoke `hello' without arguments from an ordinary shell
  prompt it prints `goodbye', rather than the expected `hello, world'.
  Here is a transcript:

  $ hello
  goodbye
  $ /usr/bin/hello
  goodbye
  $

  I suggest that the output string, in hello.c, be corrected.

  I am using Debian 1.1, kernel version 1.3.99.15z
  and libc 5.2.18.3.2.1.3-beta.
</example>
<p>

<sect1>Using dpkg to find the package and version for the report
<p>
   If you are reporting a bug in a command, you can find out which
   package installed it by using dpkg --search. You can find out which
   version of a package you have installed by using dpkg --list or dpkg
   --status.
<p>   
   For example:
<example>
$ which dpkg
/usr/bin/dpkg
$ type dpkg
dpkg is hashed (/usr/bin/dpkg)
$ dpkg --search /usr/bin/dpkg
dpkg: /usr/bin/dpkg
$ dpkg --search '*/dpkg'
dpkg: /usr/doc/copyright/dpkg
dpkg: /var/lib/dpkg
dpkg: /usr/bin/dpkg
dpkg: /usr/lib/dpkg
dpkg: /usr/doc/dpkg
$ dpkg --list dpkg
Desired=Unknown/Install/Remove/Purge
| Status=Not/Installed/Config-files/Unpacked/Failed-config/Half-installed
|/ Err?=(none)/Hold/Reinst-required/X=both-problems (Status,Err: uppercase=bad)
||/ Name            Version        Description
+++-===============-==============-============================================
ii  dpkg            1.2.9elf       Package maintenance system for Debian Linux
$ dpkg --status dpkg
Package: dpkg
Essential: yes
Status: install ok installed
Priority: required
Section: base
Maintainer: Ian Jackson &lt;ian@chiark.chu.cam.ac.uk&gt;
Version: 1.2.9elf
Replaces: dpkgname
Pre-Depends: libc5 (>= 5.2.18-2), ncurses3.0
Conflicts: dpkgname
Description: Package maintenance system for Debian Linux
 This package contains the programs which handle the installation and
 removal of packages on your system.
 .
 The primary interface for the dpkg suite is the `dselect' program;
 a more low-level and less user-friendly interface is available in
 the form of the `dpkg' command.

$
</example>
<p>

<sect1>Minor bugs
<p>
   If a bug report is minor (for example, a documentation typo or other
   trivial build problem), or you're submitting many reports at once,
   send them to <tt>maintonly@bugs.debian.org</tt> or
   <tt>quiet@bugs.debian.org</tt>. <tt>maintonly</tt> will send the
   report on to the package maintainer (provided you supply a correct
   Package line in the pseudo-header and the maintainer is known), and
   <tt>quiet</tt>
   will not forward it anywhere at all but only file it as a bug
   (useful if, for example, you are submitting many similar bugs and want
   to post only a summary).
<p>   
   If you do this the bug system will set the <tt>Reply-To</tt>
   of any forwarded
   message so that replies will by default be processed in the same way
   as the original report.
<p>

<sect1>Sending a copy of your report to other addresses
<p>
   Sometimes it is necessary to send a copy of a bug report to somewhere
   else besides the <tt>debian-devel</tt> mailing list
   and the package maintainer, which is where
   they are normally sent.
<p>
   You could do this by CC'ing your bug report to the other address(es),
   but then the other copies would not have the bug report number put in
   the <tt>Reply-To</tt> field and the <tt>Subject</tt> line of the
   mail header. When the recipients reply
   they will probably preserve the <tt>submit@bugs.debian.org</tt>
   entry in the
   header and have their message filed as a new bug report. This leads to
   many duplicated reports.
<p>   
   The <em>right</em> way to do this is to use the <tt>X-Debian-CC</tt>
   header. Add a
   line like this to your message's mail header (not to the pseudo header
   with the Package field):
<example>
 X-Debian-CC: other-list@cosmic.edu
</example>
   This will cause the bug tracking system to send a copy of your report
   to the address(es) specified in the <tt>X-Debian-CC</tt> line
   as well as to the <tt>debian-devel</tt> mailing list.
<p>   
   This feature can often be combined usefully with mailing
   <tt>quiet@bugs.debian.org</tt> - see above.
<p>   

<sect1>Unknown packages or bugs with no Package key
<p>  
   If the bug tracking system doesn't know who the maintainer of the
   relevant package is it'll forward the report to the <tt>debian-devel</tt>
   mailing list, even if <tt>maintonly</tt> was used.
<p>   
   When sending to <tt>maintonly@bugs.debian.org</tt> or to
   <tt>nnn-maintonly@bugs.debian.org</tt>
   you should make
   sure that the bug report is assigned to the right package, by putting
   a correct Package at the top of an original submission of a report, or
   by using the <tt>control@bugs.debian.org</tt>
   service to (re)assign the report
   appropriately first if it isn't correct already (see below).
<p>   

<sect1>Using the `bug' utility
<p>

[to be filled]
<p>

<sect>Getting information about reported bugs
<p>

   Each message received at or sent by the bug processing system is
   logged and made available in a number of ways.
<p>   
   Copies of the logs are available on the World Wide Web at
   <tt>http://www.cl.cam.ac.uk/users/iwj10/debian-bugs/</tt> and
   <tt>http://www.debian.org/Bugs/</tt> (<em>not up-to-date at present</em>
   [is this still correct?]).
   <p>
   The HTML files containing the bug report logs are available in the
   WebPages subdirectory of the Debian FTP archive, and will be available
   on mirror sites that haven't explicitly removed them from their mirror
   configuration. A web server which is configured to serve this part of
   the FTP area as a webtree will provide a local copy of the pages.
   <p>
   There is a mailserver which can send bug reports as plain text on
   request. To use it send the word help as the sole contents of an email
   to <tt>request@bugs.debian.org</tt>
   (the `Subject' of the message is ignored), or
   read the instructions on the World Wide Web or in the file
   <tt>bug-log-mailserver.txt</tt> [ref needs to be fixed].
<p>
<sect1>The mail interface
<p>

   There is a mailserver which can send the bug reports and indices as
   plain text on request.
<p>   
   To use it you send a mail message to <tt>request@bugs.debian.org</tt>. The
   `Subject' of the message is ignored, except for generating the `Subject'
   of the reply.
<p>   
   The body you send should be a series of commands, one per line. You'll
   receive a reply which looks like a transcript of your message being
   interpreted, with a response to each command. No notifications are
   sent to anyone for most commands; however, the messages are logged and
   made available in the WWW pages.
<p>   
   Any text on a line starting with a hash sign `#' is ignored; the server
   will stop processing when it finds a line starting with quit, stop,
   thank or two hyphens (to avoid parsing a signature). It will also stop
   if it encounters too many unrecognised or badly-formatted commands. If
   no commands are successfully handled it will send the help text for
   the server.
<p>

Here is a list of available commands:
<taglist>
<tag><tt>send bugnumber</tt>
<item><p>
<tag><tt>send-detail bugnumber</tt>
<item>
          Requests the transcript for the bug report in question.
          send-detail sends all of the `boring' messages in the
          transcript, such as the various auto-acks (you should usually
          use send as well, as the interesting messages are not sent by
          send-detail).
<tag><tt>index [full]</tt>
<item><p>
<tag><tt>index-summary by-package</tt>
<item><p>
<tag><tt>   index-summary by-number</tt>
<item>
          Request the full index (with full details, and including done
          and forwarded reports), or the summary sorted by package or by
          number, respectively.
          
<tag><tt>   index-maint</tt>
<item>
          Requests the index page giving the list of maintainers with
          bugs (open and recently-closed) in the tracking sytem.
          
<tag><tt>   index maint maintainer</tt>
<item>
          Requests the index pages of bugs in the system for all
          maintainers containing the string maintainer. The search term
          is a case insensitive substring. The bug index for each
          matching maintainer will be sent in a separate message.
          
<tag><tt>   send-unmatched [this|0]</tt>
<item><p>
          
<tag><tt>   send-unmatched last|-1</tt>
<item><p>
          
<tag><tt>   send-unmatched old|-2</tt>
<item>
          Requests logs of messages not matched to a particular bug
          report, for this week, last week and the week before. (Each
          week ends on a Wednesday.)
          
<tag><tt>   getinfo filename</tt>
<item>
          Request a file containing information about package(s) and or
          maintainer(s) - the files available are:
          
        maintainers
               The unified list of packages' maintainers, as used by the
               tracking system. This is derived from information in the
               Packages files, override files and pseudo-packages files.
        override.stable
        override.development
        override.contrib
        override.non-free
        override.experimental
        override.codeword
               Information about the priorities and sections of packages
               and overriding values for the maintainers. This
               information is used by the process which generates the
               Packages files in the FTP archive. Information is
               available for each of the main distribution trees
               available; the current stable and development trees are
               available by their codewords as well as by their release
               status.
        pseudo-packages.description
        pseudo-packages.maintainers
               List of descriptions and maintainers respectively for
               pseudo-packages.
               
<tag><tt>   refcards</tt>
<item>
          Requests that the mailservers' reference card be sent in plain
          ASCII.
          
<tag><tt>   help</tt>
<item>
          Requests that this help document be sent by email in plain
          ASCII.
          
<tag><tt>quit</tt>, <tt>stop</tt>
<item><p>
<tag><tt>   thank...</tt>
<item><p>          
<tag><tt>   --...</tt>
<item>
          Stops processing at this point of the message. After this you
          may include any text you like, and it will be ignored. You can
          use this to include longer comments than are suitable for #,
          for example for the benefit of human readers of your message
          (reading it via the tracking system logs or due to a CC or
          BCC).
          
<tag><tt>   #... </tt>
<item>
          One-line comment. The # must be at the start of the line.
          
<tag><tt>   debug level</tt>
<item>
          Sets the debugging level to level, which should be a
          nonnegative integer. 0 is no debugging; 1 is usually
          sufficient. The debugging output appears in the transcript. It
          is not likely to be useful to general users of the bug system.
</taglist>          
   There is a reference card for the mailservers, available via the WWW,
   in bug-mailserver-refcard.txt or by email using the refcard command
   (see above).[to be fixed]
<p>   
   If you wish to manipulate bug reports you should use the
   <tt>control@bugs.debian.org</tt>
   address, which understands a superset of the
   commands listed above. This is described in another document,
   available on the WWW, in the file bug-maint-mailcontrol.txt, or by
   sending help to control@bugs.[to be fixed]
<p>   
   In case you are reading this as a plain text file or via email: an
   HTML version is available via the bug system main contents page
   <tt>http://www.debian.org/Bugs/</tt>.
<p>   
   Suggestions for future additions are welcome. Please mail
   owner@bugs.debian.org, debian-user@lists.debian.org or
   debian-devel@lists.debian.org.[to be fixed?]
<p>   

<sect1>The WWW interface
<p>

[to be filled]

</book>
